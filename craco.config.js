/* The CRACO config for esbuild.
 *
 * https://github.com/pradel/create-react-app-esbuild/tree/main/packages/craco-esbuild
 */
const CracoEsbuildPlugin = require("craco-esbuild");

module.exports = {
    plugins: [{
      plugin: CracoEsbuildPlugin,
    }],
};
