import Typography from "@mui/material/Typography";
import { makeStyles } from "@mui/styles";

import React from "react";

import { Switch, Route } from "react-router-dom";

import Header from "./Header";
import Workflow from "./Workflow";
import About from "./About";

const useStyles = makeStyles(() => ({
    tbd: {
        "padding-top": "60px",
    },
}));

/*
 * A React component that contains the framework for the site.
 */
export default function App(): JSX.Element {
    return (
        <div>
            <Header />
            <Switch>
                <Route exact path="/">
                    <Workflow />
                </Route>
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/partners">
                    <TBD />
                </Route>
                <Route path="/projects">
                    <TBD />
                </Route>
                <Route path="/get-involved">
                    <TBD />
                </Route>
            </Switch>
        </div>
    );
}

function TBD(): JSX.Element {
    const { tbd } = useStyles();

    return (
        <div className={tbd}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
                A bit of a work In Progress. Come back soon!
            </Typography>
        </div>
    );
}
