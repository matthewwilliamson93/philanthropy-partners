export const LEGEND_DATA = {
    cp: {
        tag: "CP",
        name: "Corporate Philanthropy",
        color: "#8DD3C7",
    },
    pp: {
        tag: "PP",
        name: "Philanthropy Partners",
        color: "#FFFFB3",
    },
    sh: {
        tag: "SH",
        name: "Stakeholders",
        color: "#BEBADA",
    },
    v: {
        tag: "V",
        name: "Volunteers",
        color: "#FB8072",
    },
};

export const NODES = [
    {
        id: "1",
        data: {
            label: "Identify",
            tabs: [
                {
                    tag: "cp",
                    description: [
                        "Gather different projects from our partners to work on",
                    ],
                },
                {
                    tag: "sh",
                    description: [
                        "Present different project ideas to Philanthropy",
                    ],
                },
            ],
        },
        position: { x: 100, y: 100 },
    },
    {
        id: "2",
        data: {
            label: "Filter projects / Define Scope",
            tabs: [
                {
                    tag: "pp",
                    description: [
                        "Use expertise to determine size and scope of suggested projects",
                        "Filter for appropriate / completable projects",
                    ],
                },
                {
                    tag: "sh",
                    description: [],
                },
                {
                    tag: "cp",
                    description: [],
                },
            ],
        },
        position: { x: 200, y: 250 },
    },
    {
        id: "3",
        data: {
            label: "Create a Plan",
            tabs: [
                {
                    tag: "pp",
                    description: [
                        "Provide a clear plan on how one could go about implementing this project",
                        "Break down larger projects into smaller, parts with clear deliverables",
                        "Break down deliverables into well-defined tasks tagged with required skills",
                        "What kinds of entities are needed ie. Front end screen, backend service, DB, etc",
                        "Where can that be hosted?",
                        "What skills would be needed to work on this project?",
                        "What is the size of the work?",
                    ],
                },
            ],
        },
        position: { x: 200, y: 500 },
    },
    {
        id: "4",
        data: {
            label: "Review Plan",
            tabs: [
                {
                    tag: "pp",
                    description: [
                        "Confirm plan with Philanthropy and Stakeholders",
                    ],
                },
                {
                    tag: "cp",
                    description: [],
                },
                {
                    tag: "sh",
                    description: [],
                },
            ],
        },
        position: { x: 350, y: 350 },
    },
    {
        id: "5",
        position: { x: 500, y: 500 },
        data: {
            label: "Assign Project",
            tabs: [
                {
                    tag: "pp",
                    description: [
                        "Find people interested in working on these kinds of projects",
                        "Philanthropy interests e.g. climate oriented, mental health, etc",
                        "Time available to commit",
                        "Current set of skills",
                        "What they want to learn",
                        "What they are interested in",
                        "Once selected we would need to debrief a volunteer to help lead this project, review the plan with them, and confirm there commitment",
                    ],
                },
                {
                    tag: "v",
                    description: [],
                },
            ],
        },
    },
    {
        id: "6",
        data: {
            label: "Implement Project",
            tabs: [
                {
                    tag: "v",
                    description: [
                        "Implement the project",
                        "Manage volunteers on the project and coordiate with Philanthropy Partners regarding progress",
                    ],
                },
                {
                    tag: "pp",
                    description: [
                        "Identify and resolve any dependencies needed before a project can be implemented",
                        "Be able to assist volunteers with questions that may come up",
                        "Maintain responsibility for assessing and communicating progress",
                    ],
                },
            ],
        },
        position: { x: 600, y: 650 },
    },
    {
        id: "7",
        data: {
            label: "Show off Project",
            tabs: [
                {
                    tag: "v",
                    description: [],
                },
                {
                    tag: "pp",
                    description: [],
                },
                {
                    tag: "sh",
                    description: [],
                },
                {
                    tag: "cp",
                    description: [],
                },
            ],
        },
        position: { x: 700, y: 800 },
    },
    {
        id: "8",
        data: {
            label: "Hand it off",
            tabs: [
                {
                    tag: "sh",
                    description: [
                        "All projects are should be designed to eventually be handed off",
                        "Take over the project",
                    ],
                },
                {
                    tag: "pp",
                    description: [
                        "Make sure the project is understood and can be taken on by the Philanthropy",
                    ],
                },
                {
                    tag: "cp",
                    description: [],
                },
            ],
        },
        position: { x: 700, y: 900 },
    },
];

export const EDGES = [
    {
        id: "e1-2",
        source: "1",
        target: "2",
    },
    {
        id: "e2-3",
        source: "2",
        target: "3",
    },
    {
        id: "e3-4",
        source: "3",
        target: "4",
    },
    {
        id: "e4-2",
        source: "4",
        target: "2",
        animated: true,
        label: "Iterate with Philanthropies",
    },
    {
        id: "e4-5",
        source: "4",
        target: "5",
    },
    {
        id: "e5-6",
        source: "5",
        target: "6",
    },
    {
        id: "e6-7",
        source: "6",
        target: "7",
    },
    {
        id: "e7-6",
        source: "7",
        target: "6",
        animated: true,
        label: "Iterate with Philanthropies",
    },
    {
        id: "e7-8",
        source: "7",
        target: "8",
    },
];
