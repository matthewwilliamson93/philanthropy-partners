import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import Typography from "@mui/material/Typography";

import React, { Dispatch, SetStateAction, useState } from "react";

interface Tab {
    tag: string;
    name: string;
    description: Array<string>;
}

export interface PopupData {
    label: string;
    tabs: Array<Tab>;
}

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 2 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

const style = {
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    display: "flex",
    flexGrow: 1,
    position: "absolute" as const,
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    height: 400,
    width: 800,
    p: 4,
};

/*
 * A React component that creates a Popup for visualizing different parts
 * of a workflow.
 *
 * https://mui.com/components/modal/
 * https://mui.com/components/tabs/#vertical-tabs
 */
export default function Popup({
    open,
    setOpen,
    data,
}: {
    open: boolean;
    setOpen: Dispatch<SetStateAction<boolean>>;
    data: PopupData | null;
}): JSX.Element {
    const [value, setValue] = useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    if (data === null) {
        return <div />;
    }

    return (
        <div>
            <Modal
                open={data !== null && open}
                onClose={() => {
                    setValue(0);
                    setOpen(false);
                }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Tabs
                        orientation="vertical"
                        variant="scrollable"
                        value={value}
                        onChange={handleChange}
                        aria-label="Groups involved"
                        sx={{ borderRight: 1, borderColor: "divider" }}
                    >
                        {data.tabs.map((tab, index) => (
                            <Tab
                                label={tab.name}
                                id={`vertical-tab-${index}`}
                                aria-controls={`vertical-tabpanel-${index}`}
                                key={index}
                            />
                        ))}
                    </Tabs>
                    {data.tabs.map((tab, index) => (
                        <TabPanel value={value} index={index} key={index}>
                            <Typography
                                id="modal-modal-title"
                                variant="h6"
                                component="h2"
                            >
                                {`${data.label} - ${data.tabs[index].name}`}
                            </Typography>
                            <Typography
                                id="modal-modal-description"
                                sx={{ mt: 2 }}
                            >
                                <ul>
                                    {data.tabs[index].description.map(
                                        (note, index) => (
                                            <li
                                                id={`modal-modal-description-${index}`}
                                                key={`modal-modal-description-${index}`}
                                            >
                                                {note}
                                            </li>
                                        )
                                    )}
                                </ul>
                            </Typography>
                        </TabPanel>
                    ))}
                </Box>
            </Modal>
        </div>
    );
}
